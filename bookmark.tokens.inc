<?php

/**
 * @file
 * Token integration for the bookmark module.
 */

/**
 * Implements hook_token_info().
 */
function bookmark_token_info() {
  $info['types']['bookmark'] = array(
    'name' => t('Bookmark'),
    'description' => t('Tokens related to individual bookmarks.'),
    'needs-data' => 'bookmark',
  );

  $info['tokens']['bookmark']['bid'] = array(
    'name' => t('Bookmark ID'),
    'description' => t('The unique ID of the bookmark.'),
  );
  $info['tokens']['bookmark']['title'] = array(
    'name' => t('Title'),
    'description' => t('The title of the bookmark.'),
  );
  $info['tokens']['bookmark']['language'] = array(
    'name' => t('Language'),
    'description' => t('The language of the bookmark.'),
  );
  $info['tokens']['bookmark']['url'] = array(
    'name' => t('URL'),
    'description' => t('The URL of the bookmark.'),
  );
  $info['tokens']['bookmark']['edit-url'] = array(
    'name' => t('Edit URL'),
    'description' => t("The URL of the bookmark's edit page."),
    'type' => 'url',
  );
  $info['tokens']['bookmark']['created'] = array(
    'name' => t('Date created'),
    'description' => t('The date the bookmark was created.'),
    'type' => 'date',
  );
  $info['tokens']['bookmark']['changed'] = array(
    'name' => t('Date changed'),
    'description' => t('The date the bookmark was most recently updated.'),
    'type' => 'date',
  );
  $info['tokens']['bookmark']['user'] = array(
    'name' => t('User'),
    'description' => t('The user account that owns the bookmark.'),
    'type' => 'user',
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function bookmark_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $url_options = array('absolute' => TRUE);

  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  }
  else {
    $language_code = NULL;
  }

  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'bookmark' && !empty($data['bookmark'])) {
    $bookmark = $data['bookmark'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'bid':
          $replacements[$original] = $bookmark->bid;
          break;
        case 'title':
          $replacements[$original] = $sanitize ? check_plain($bookmark->title) : $bookmark->title;
          break;

        case 'body':
        case 'summary':
          if (!empty($node->body)) {
            $item = $node->body[$node->language][0];
            $column = ($name == 'body') ? 'value' : 'summary';
            $instance = field_info_instance('node', 'body', $node->type);
            $replacements[$original] = $sanitize ? _text_sanitize($instance, $node->language, $item, $column) : $item[$column];
          }
          break;

        case 'language':
          $replacements[$original] = $sanitize ? check_plain($node->language) : $node->language;
          break;

        case 'url':
          $replacements[$original] = url('node/' . $node->nid, $url_options);
          break;

        case 'edit-url':
          $replacements[$original] = url('node/' . $node->nid . '/edit', $url_options);
          break;

        // Default values for the chained tokens handled below.
        case 'author':
          $name = ($node->uid == 0) ? variable_get('anonymous', t('Anonymous')) : $node->name;
          $replacements[$original] = $sanitize ? filter_xss($name) : $name;
          break;

        case 'created':
          $replacements[$original] = format_date($node->created, 'medium', '', NULL, $language_code);
          break;

        case 'changed':
          $replacements[$original] = format_date($node->changed, 'medium', '', NULL, $language_code);
          break;
      }
    }

    if ($author_tokens = token_find_with_prefix($tokens, 'author')) {
      $author = user_load($node->uid);
      $replacements += token_generate('user', $author_tokens, array('user' => $author), $options);
    }

    if ($created_tokens = token_find_with_prefix($tokens, 'created')) {
      $replacements += token_generate('date', $created_tokens, array('date' => $node->created), $options);
    }

    if ($changed_tokens = token_find_with_prefix($tokens, 'changed')) {
      $replacements += token_generate('date', $changed_tokens, array('date' => $node->changed), $options);
    }
  }

  return $replacements;
}
